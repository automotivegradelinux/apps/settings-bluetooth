##Wifi Manager
This folder include Bluetooth manager binding and GUI.

##Function
- Power on/off
- Scan start/stop
- Pair
- Connect (After pairing, support incoming connect as well)
- Disconnect
- Remove

##Guidance for building Bluetooth manager Binding
A. Prepare sdk environment
   - bitbake below:
     $ bitbake agl-demo-platform-crosssdk
   - install sdk
     $ build/tmp/deploy/sdk/poky-agl-glibc*.sh
B. Build
   - Go to folder where Bluetooth Bindings source code exists
   - run below command for setup and make
     $ . /opt/poky-agl/xxx/environment-setup-cortexa15hf-vfp-neon-poky-linux-gnueabi
     $ mkdir build
     $ cd build
     $ cmake ..
     $ make
     <widget wll be generated>
C. Running app
   - copy widget to target board
   - do install and start the app
     # afm-util install *.wgt
     # afm-util start <apps's name>@0.1

##TODO
- ADD token security
- ADD event receive

